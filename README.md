# Getting Started with Spring, Maven and Docker

## Prerequisite

- JDK 17.0.6
- Maven 3.8.1
- Intellij IDEA
- Docker Desktop


## Clone the repo

```
git clone https://gitlab.com/springbootdevops/springbootdocker.git
```


## Execute the below command to build the package:


```
./mvnw package
```

## Running the app packages as JAR file


```
java -jar target/spring-boot-docker-0.0.1-SNAPSHOT.jar 
```

You can now access the Hello World page via the web browser  http://localhost:8080 

